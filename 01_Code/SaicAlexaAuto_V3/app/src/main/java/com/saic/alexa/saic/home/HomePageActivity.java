package com.saic.alexa.saic.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;


import com.rd.PageIndicatorView;
import com.saic.alexa.R;
import com.saic.alexa.saic.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;


public class HomePageActivity extends BaseActivity {
    private ViewPager mViewPager;
    private PageIndicatorView mPageIndicatorView;
    private List<Fragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        mViewPager = findViewById(R.id.saic_viewPager);
        mPageIndicatorView = findViewById(R.id.pageIndicatorView);
        setTitle(R.string.things_to_try);
        initPager();
    }

    private void initPager() {
        // first page
        List<MenuBean> firstPage = new ArrayList<>();
        firstPage.add(new MenuBean(R.string.menu_getting_started, R.drawable.ic_getting_started, MenuBean.Type.GETTING_START));
        firstPage.add(new MenuBean(R.string.menu_alxa_talents, R.drawable.ic_alexatalents, MenuBean.Type.ALEXA_TALENT));
        firstPage.add(new MenuBean(R.string.menu_entertainment, R.drawable.ic_entertainment, MenuBean.Type.ENTERTAINMENT));
        firstPage.add(new MenuBean(R.string.menu_coumunication, R.drawable.ic_communication, MenuBean.Type.COMMUNICATION));
        firstPage.add(new MenuBean(R.string.menu_weather, R.drawable.ic_weather, MenuBean.Type.WEATHER));
        firstPage.add(new MenuBean(R.string.menu_navigation, R.drawable.ic_navigation, MenuBean.Type.NAVIGATION));
        fragments.add(PagerFragment.newInstance(firstPage));

        // second page
        List<MenuBean> secondPage = new ArrayList<>();
        secondPage.add(new MenuBean(R.string.menu_music, R.drawable.ic_music, MenuBean.Type.MUSIC));
        secondPage.add(new MenuBean(R.string.menu_news_infomation, R.drawable.ic_news_and_information, MenuBean.Type.NEWS));
        secondPage.add(new MenuBean(R.string.menu_traffic, R.drawable.ic_traffic_information, MenuBean.Type.TRAFFIC_INFOMATION));
        fragments.add(PagerFragment.newInstance(secondPage));

        mViewPager.setAdapter(new HomePagerAdapter(getSupportFragmentManager()));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPageIndicatorView.setSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    class HomePagerAdapter extends FragmentPagerAdapter {

        public HomePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
