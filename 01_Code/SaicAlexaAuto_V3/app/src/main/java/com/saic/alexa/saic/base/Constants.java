package com.saic.alexa.saic.base;

public interface Constants {
    // weathter
    String TEMPLATE_DATA = "template_data";
    String TEMPLATE_TYPE = "template_type";
    String TITLE_ARGS = "title";
    String PROMPT_ARGS = "prompt";
    String VOICE_CHROME_STATE = "voice_chrome_state";
    String FROM_TOUCH = "fromtouch";

    /**
     * 是否启用service来启动alexa引擎
     */
    boolean START_SERVICE_CONFIG = false;

    /**
     * 是否切换到sample中运行
     */
    boolean SHOW_SAMPLE_DEMO = false;

    String KEY_TITLE="title";
    String KEY_CONTENT="content";
    String KEY_KEY="key";
}
