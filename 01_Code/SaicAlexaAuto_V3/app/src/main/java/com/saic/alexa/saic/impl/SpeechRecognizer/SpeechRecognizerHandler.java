/*
 * Copyright 2017-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.saic.alexa.saic.impl.SpeechRecognizer;

import android.content.Context;

import com.amazon.aace.alexa.AlexaClient;
import com.amazon.aace.alexa.SpeechRecognizer;
import com.amazon.autovoicechrome.util.AutoVoiceChromeState;
import com.saic.alexa.saic.impl.AlexaClient.AlexaClientHandler;
import com.saic.alexa.saic.impl.Logger.LoggerHandler;
import com.saic.alexa.saic.util.BroadcastUtil;

import java.util.Observable;
import java.util.Observer;

public class SpeechRecognizerHandler extends SpeechRecognizer {

    private static final String TAG = SpeechRecognizerHandler.class.getSimpleName();
    private static SpeechRecognizerHandler mSpeechRecognizerHandler;

    private final Context mActivity;
    private final LoggerHandler mLogger;
    private final AlexaClientHandler mAlexaClient;
    private AudioCueObservable mAudioCueObservable = new AudioCueObservable();
    private boolean mAllowStopCapture = false; // Only true if holdToTalk() returned true

    public SpeechRecognizerHandler(AlexaClientHandler alexaClient,
                                   Context activity,
                                   LoggerHandler logger,
                                   boolean wakeWordEnabled) {
        super(wakeWordEnabled);
        mActivity = activity;
        mLogger = logger;
        mAlexaClient = alexaClient;
        mSpeechRecognizerHandler = this;
    }

    public static SpeechRecognizerHandler getInstance() {
        if (mSpeechRecognizerHandler == null) {
            throw new RuntimeException("SpeechRecognizerHandler is null!");
        }
        return mSpeechRecognizerHandler;
    }

    @Override
    public boolean wakewordDetected(String wakeWord) {
        mAudioCueObservable.playAudioCue(AudioCueState.START_VOICE);

        // 监听到关键词，启动监听界面
        goToResultActivity();

        // Notify Error state to AutoVoiceChrome if disconnected with Alexa
        if (mAlexaClient.getConnectionStatus() != AlexaClient.ConnectionStatus.CONNECTED) {
            BroadcastUtil.sendAutoVoiceChromeState(mActivity, AutoVoiceChromeState.IN_ERROR);
        }
        return true;
    }

    private void goToResultActivity() {
        BroadcastUtil.sendWakeWordDedected(mActivity);
    }

    @Override
    public void endOfSpeechDetected() {
        mAudioCueObservable.playAudioCue(AudioCueState.END);
    }

    public void onTapToTalk() {
        if (tapToTalk()) {
            mAudioCueObservable.playAudioCue(AudioCueState.START_TOUCH);
        }
    }

    public void onHoldToTalk() {
        mAllowStopCapture = false;
        if (holdToTalk()) {
            mAllowStopCapture = true;
            mAudioCueObservable.playAudioCue(AudioCueState.START_TOUCH);
        }
    }

    public void onReleaseHoldToTalk() {
        if (mAllowStopCapture) {
            stopCapture();
        }
        mAllowStopCapture = false;
    }


    /* For playing speech recognition audio cues */

    public enum AudioCueState {START_TOUCH, START_VOICE, END}

    public static class AudioCueObservable extends Observable {

        void playAudioCue(AudioCueState state) {
            setChanged();
            notifyObservers(state);
        }
    }

    public void addObserver(Observer observer) {
        if (mAudioCueObservable == null) {
            mAudioCueObservable = new AudioCueObservable();
        }
        mAudioCueObservable.addObserver(observer);
    }
}
