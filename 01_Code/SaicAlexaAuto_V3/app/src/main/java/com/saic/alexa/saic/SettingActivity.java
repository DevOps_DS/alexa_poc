package com.saic.alexa.saic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.saic.alexa.R;
import com.saic.alexa.saic.base.BaseActivity;
import com.saic.alexa.saic.base.Constants;
import com.saic.alexa.saic.home.HomePageActivity;
import com.saic.alexa.saic.util.SpUtil;

public class SettingActivity extends BaseActivity implements View.OnClickListener {
    TextView mHandsOn;
    TextView mDefaultVoiceOnOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setTitle(R.string.saic_settings_cap);
        findViewById(R.id.ll_contract).setOnClickListener(this);
        findViewById(R.id.ll_hands_on_free).setOnClickListener(this);
        findViewById(R.id.ll_default_voice).setOnClickListener(this);

        mHandsOn = findViewById(R.id.tv_handson_off);
        mDefaultVoiceOnOff = findViewById(R.id.tv_default_voice_off);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_contract:

                break;
            case R.id.ll_hands_on_free:
                Intent intent = new Intent(mContext, SettingOnOffActivity.class);
                intent.putExtra(Constants.KEY_TITLE, getString(R.string.saic_hands_free_upper));
                intent.putExtra(Constants.KEY_KEY, "handson");
                intent.putExtra(Constants.KEY_CONTENT, getString(R.string.saic_hands_on_prompt));
                mContext.startActivity(intent);
                break;
            case R.id.ll_default_voice:
                Intent voice = new Intent(mContext, SettingOnOffActivity.class);
                voice.putExtra("title", getString(R.string.saic_default_voice_upper));
                voice.putExtra("key", "default_voice");
                voice.putExtra("content", getString(R.string.saic_default_voice_prompt));
                mContext.startActivity(voice);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        boolean handsOnOff = SpUtil.getValue(mContext, "handson");
        mHandsOn.setText(handsOnOff ? getString(R.string.saic_on) : getString(R.string.saic_off));
        boolean defaultVoiceOnOff = SpUtil.getValue(mContext, "default_voice");
        mDefaultVoiceOnOff.setText(defaultVoiceOnOff ? getString(R.string.saic_on) : getString(R.string.saic_off));
    }
}
