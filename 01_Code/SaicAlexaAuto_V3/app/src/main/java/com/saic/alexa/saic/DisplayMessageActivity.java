package com.saic.alexa.saic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;


import com.saic.alexa.R;

import static android.text.Html.FROM_HTML_MODE_LEGACY;

public class DisplayMessageActivity extends AppCompatActivity {

    private TextView urlTextView;
    private TextView codeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("alexa-cbl-expire");
        intentFilter.addAction("alexa-cbl-cancel");
        intentFilter.addAction("alexa-cbl-code");
        intentFilter.addAction("alexa-cbl-refreshed");

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, intentFilter);

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        showCBLCode(getIntent());

        findViewById(R.id.cancelButton).setOnClickListener(v -> sendMessage());
        findViewById(R.id.retryButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRetryMessage();
            }
        });
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("alexa-cbl-cancel".equals(intent.getAction())) {
                DisplayMessageActivity.this.finish();
            } else if ("alexa-cbl-expire".equals(intent.getAction())) {
                showCblExpireMsg();
            } else if ("alexa-cbl-code".equals(intent.getAction())) {
                showCBLCode(intent);
            } else if ("alexa-cbl-refreshed".equals(intent.getAction())) {
                DisplayMessageActivity.this.finish();
            }
        }
    };

    private void showCBLCode(Intent intent) {
        findViewById(R.id.cblCodeText).setVisibility(View.VISIBLE);
        findViewById(R.id.retryButton).setVisibility(View.GONE);

        String url = intent.getStringExtra("url");
        String code = intent.getStringExtra("code");
        urlTextView = findViewById(R.id.cblCodeView);
        codeTextView = findViewById(R.id.cblCodeText);

        urlTextView.setText(Html.fromHtml(getString(R.string.alexa_cbl, url), FROM_HTML_MODE_LEGACY));
        codeTextView.setText(code);
    }

    private void showCblExpireMsg() {
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }

        urlTextView = findViewById(R.id.cblCodeView);
        urlTextView.setText(getString(R.string.cbl_code_expire));

        findViewById(R.id.cblCodeText).setVisibility(View.GONE);
        findViewById(R.id.retryButton).setVisibility(View.VISIBLE);
    }

    private void sendMessage() {
        Intent intent = new Intent("alexa-cbl");
        intent.putExtra("message", "cancel");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        DisplayMessageActivity.this.finish();
    }

    private void sendRetryMessage() {
        Intent intent = new Intent("alexa-cbl");
        intent.putExtra("message", "retry");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }
}
