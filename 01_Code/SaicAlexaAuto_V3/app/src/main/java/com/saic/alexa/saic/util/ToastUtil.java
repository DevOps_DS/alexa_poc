package com.saic.alexa.saic.util;


import android.content.Context;
import android.widget.Toast;

public class ToastUtil {

    /**
     * show short time toast
     *
     * @param context
     * @param msg
     */
    public static void showShort(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * show long time toast
     *
     * @param context
     * @param msg
     */
    public static void showLong(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}
