package com.saic.alexa.saic.impl.AlexaSpeaker;

import android.content.Context;

import com.amazon.aace.alexa.AlexaSpeaker;
import com.saic.alexa.saic.impl.Logger.LoggerHandler;

public class AlexaSpeakerHandler extends AlexaSpeaker {
    private static final String TAG = AlexaSpeaker.class.getSimpleName();

    private final Context mActivity;
    private final LoggerHandler mLogger;

    private boolean mIsMuted = false;
    private byte mAlexaVolume = 50;
    private byte mAlertsVolume = 50;

    public AlexaSpeakerHandler(Context Context, LoggerHandler logger) {
        mActivity = Context;
        mLogger = logger;
    }

    @Override
    public void speakerSettingsChanged(SpeakerType type, boolean local, byte volume, boolean mute) {
        mLogger.postInfo(TAG, String.format("speakerSettingsChanged [type=%s,local=%b,volume=%d,mute=%b]", type.toString(), local, volume, mute));

        if (type == SpeakerType.ALEXA_VOLUME) {
            mAlexaVolume = volume;
            mIsMuted = mute;
        } else if (type == SpeakerType.ALERTS_VOLUME) {
            mAlertsVolume = volume;
        }

    }

}