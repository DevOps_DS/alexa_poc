package com.saic.alexa.saic.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.amazon.autovoicechrome.util.AutoVoiceChromeState;
import com.saic.alexa.saic.base.Constants;

/**
 * @author xulei
 * @time 2020-04-16
 */
public class BroadcastUtil {
    /**
     * 更新状态指示器的状态
     *
     * @param context
     * @param state
     */
    public static void sendAutoVoiceChromeState(Context context, AutoVoiceChromeState state) {
        if (null == state) {
            return;
        }
        String value = state.name();
        Intent intent = new Intent();
        intent.setAction("com.saic.alexa.refresh.chrome.state");
        intent.putExtra("state", value);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void registerAutoVoiceChromeStateBroadcast(Context context, BroadcastReceiver receiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.saic.alexa.refresh.chrome.state");
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, intentFilter);
    }

    public static void unregisterBroadcast(Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    /**
     * 监听到关键词时，发送广播，启动监听显示界面
     *
     * @param context
     */
    public static void sendWakeWordDedected(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.saic.alexa.wake.word.detected");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    /**
     * 注册结果页面启动监听
     *
     * @param context
     * @param receiver
     */
    public static void registerRequestResultBroadcast(Context context, BroadcastReceiver receiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.saic.alexa.wake.word.detected");
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, intentFilter);
    }

    /**
     * 注册json广播
     *
     * @param context
     * @param receiver
     */
    public static void registerJsonResultShowBroadcast(Context context, BroadcastReceiver receiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.saic.alexa.json.result");
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, intentFilter);
    }

    /**
     * 接收到json数据，发送广播，显示在监听界面
     *
     * @param context
     * @param json
     * @param type
     */
    public static void sendJsonResult(Context context, String json, int type) {
        Intent intent = new Intent();
        intent.setAction("com.saic.alexa.json.result");
        intent.putExtra(Constants.TEMPLATE_DATA, json);
        intent.putExtra(Constants.TEMPLATE_TYPE, type);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    /**
     * 注册MainActivity销毁的监听
     *
     * @param context
     * @param receiver
     */
    public static void registerMainActivityFinish(Context context, BroadcastReceiver receiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.saic.finish.main.activity");
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, intentFilter);
    }

    public static void sendDestoryMainActivity(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.saic.finish.main.activity");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


    /**
     * 注册停止播放语音广播
     *
     * @param context
     * @param receiver
     */
    public static void registerStopSpeakingBroadcast(Context context, BroadcastReceiver receiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.saic.alexa.stop.speaking");
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, intentFilter);
    }

    /**
     * 接收到停止播放信号，发送广播
     *
     * @param context
     */
    public static void sendStopSpeaking(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.saic.alexa.stop.speaking");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
