package com.saic.alexa.saic;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;


import com.amazon.sampleapp.core.FileUtils;
import com.saic.alexa.BuildConfig;
import com.saic.alexa.LVCInteractionService;
import com.saic.alexa.R;
import com.saic.alexa.saic.base.BaseActivity;
import com.saic.alexa.saic.impl.CBL.CBLHandler;
import com.saic.alexa.saic.util.BroadcastUtil;
import com.saic.alexa.saic.util.LogUtil;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String sDeviceConfigFile = "app_config.json";
    private static final int sPermissionRequestCode = 0;
    private static final String[] sRequiredPermissions = {Manifest.permission.RECORD_AUDIO,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE};

    // Shared Preferences
    private SharedPreferences mPreferences;

    private BroadcastReceiver mFinishReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent startIntent = new Intent(mContext, SaicMainActivity.class);
            mContext.startActivity(startIntent);
            mContext.finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setShowTitle(false);
        // Check if permissions are missing and must be requested
        ArrayList<String> requests = new ArrayList<>();

        for (String permission : sRequiredPermissions) {
            if (ActivityCompat.checkSelfPermission(this, permission)
                    == PackageManager.PERMISSION_DENIED) {
                requests.add(permission);
            }
        }

        // Request necessary permissions if not already granted, else start app
        if (requests.size() > 0) {
            ActivityCompat.requestPermissions(this,
                    requests.toArray(new String[requests.size()]), sPermissionRequestCode);
        } else {
            create();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == sPermissionRequestCode) {
            if (grantResults.length > 0) {
                for (int grantResult : grantResults) {
                    if (grantResult == PackageManager.PERMISSION_DENIED) {
                        // Permission request was denied
                        Toast.makeText(this, "Permissions required",
                                Toast.LENGTH_LONG).show();
                    }
                }
                // Permissions have been granted. Start app
                create();
            } else {
                // Permission request was denied
                Toast.makeText(this, "Permissions required", Toast.LENGTH_LONG).show();
            }
        }
    }

    // 获取权限后，执行
    private void create() {
        setContentView(R.layout.saic_activity_main);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("alexa-cbl"));
        BroadcastUtil.registerMainActivityFinish(this, mFinishReceiver);
        initLVC();
        mPreferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        // Retrieve device config from config file and update preferences
        String clientId = "", productId = "", productDsn = "";
        JSONObject config = FileUtils.getConfigFromFile(getAssets(), sDeviceConfigFile, "config");
        if (config != null) {
            try {
                clientId = config.getString("clientId");
                productId = config.getString("productId");
            } catch (JSONException e) {
                Log.w(TAG, "Missing device info in app_config.json");
            }
            try {
                productDsn = config.getString("productDsn");
            } catch (JSONException e) {
                try {
                    // set Android ID as product DSN
                    productDsn = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    Log.i(TAG, "android id for DSN: " + productDsn);
                } catch (Error error) {
                    productDsn = UUID.randomUUID().toString();
                    Log.w(TAG, "android id not found, generating random DSN: " + productDsn);
                }
            }
        }
        updateDevicePreferences(clientId, productId, productDsn);

        // 登录
        this.findViewById(R.id.loginButton).setOnClickListener(v -> {
            LogUtil.i(TAG, "Starting CBL login flow...");
            CBLHandler.getInstance().startLogin();
        });

    }

    private void updateDevicePreferences(String clientId,
                                         String productId,
                                         String productDsn) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(getString(R.string.preference_client_id), clientId);
        editor.putString(getString(R.string.preference_product_id), productId);
        editor.putString(getString(R.string.preference_product_dsn), productDsn);
        editor.apply();
    }

    /**
     * Start {@link LVCInteractionService}, the service that initializes and communicates with LVC,
     * and register a broadcast receiver to receive the configuration from LVC provided through the
     * {@link LVCInteractionService}
     */
    private void initLVC() {
        // Start LVCInteractionService to communicate with LVC
        startService(new Intent(this, LVCInteractionService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BroadcastUtil.unregisterBroadcast(this,mFinishReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("alexa-cbl".equals(intent.getAction())) {
                String message = intent.getStringExtra("message");
                if (message.equals("logout")) {
                    CBLHandler.getInstance().resetCbl();
                } else if (message.equals("cancel")) {
                    CBLHandler.getInstance().cancelCbl();
                } else if (message.equals("retry")) {
                    CBLHandler.getInstance().startCbl();
                }
            }
        }
    };
}
