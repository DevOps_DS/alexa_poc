package com.saic.alexa.saic.data;

import android.app.Activity;

import com.saic.alexa.saic.util.LogUtil;

import java.util.Stack;

public class DataModel {
    private static volatile DataModel mDataModel;
    private boolean mTapClick = true;
    private boolean mIsRunning;
    private Stack<Activity> mActivities = new Stack<>();
    private boolean mLogin;

    public static DataModel getInstance() {
        if (mDataModel == null) {
            synchronized (DataModel.class) {
                if (mDataModel == null) {
                    mDataModel = new DataModel();
                }
            }
        }
        return mDataModel;
    }

    private DataModel() {}


    /**
     * 把Activity放入栈中
     *
     * @param activity
     */
    public void addActivity(Activity activity) {
        mActivities.push(activity);
    }

    /**
     * 清除栈
     */
    public void clearActivity() {
        mActivities.clear();
    }

    /**
     * 清除某个Activity
     *
     * @param activity
     */
    public void removeActivity(Activity activity) {
        if (mActivities.contains(activity)) {
            activity.finish();
            mActivities.remove(activity);
        }
    }

    /**
     * 结束所有的Activity
     */
    public void finishActivities() {
        for (Activity activity : mActivities) {
            if (activity != null) {
                LogUtil.e("datamodel", "finish activity=" + activity.getClass().getCanonicalName());
                activity.finish();
            }
        }
        // 清楚所有的Activity记录,防止内存泄漏
        clearActivity();
    }

    /**
     * 结束最新的Activity
     */
    public void finishTopActivity() {
        Activity pop = mActivities.pop();
        if (null != pop) {
            pop.finish();
        }
    }

    public void setLogin(boolean b) {
        this.mLogin = b;
    }

    public boolean isLogin() {
        return mLogin;
    }

}
