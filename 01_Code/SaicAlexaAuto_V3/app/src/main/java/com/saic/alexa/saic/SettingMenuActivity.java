package com.saic.alexa.saic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.saic.alexa.R;
import com.saic.alexa.saic.base.BaseActivity;
import com.saic.alexa.saic.home.HomePageActivity;
import com.saic.alexa.saic.impl.CBL.CBLHandler;

public class SettingMenuActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_menu);
        setTitle(R.string.saic_main);
        findViewById(R.id.tv_setting).setOnClickListener(this);
        findViewById(R.id.tv_tings_to_try).setOnClickListener(this);
        findViewById(R.id.tv_feed_back).setOnClickListener(this);
        findViewById(R.id.tv_sign_out).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.tv_setting:
                intent=new Intent(mContext, SettingActivity.class);
                break;
            case R.id.tv_tings_to_try:
                intent = new Intent(mContext, HomePageActivity.class);
                break;
            case R.id.tv_feed_back:

                break;
            case R.id.tv_sign_out:
                intent = new Intent(mContext, LogOutActivity.class);
                break;
            default:
                break;
        }
        if (intent == null) {
            return;
        }
        startActivity(intent);
    }

}
