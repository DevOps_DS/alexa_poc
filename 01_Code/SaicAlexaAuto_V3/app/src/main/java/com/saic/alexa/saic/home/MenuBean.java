package com.saic.alexa.saic.home;

public class MenuBean {
    public int name;
    public int icon;
    public Type type;

    public static enum Type{
        GETTING_START,
        ALEXA_TALENT,
        ENTERTAINMENT,
        COMMUNICATION,
        WEATHER,
        NAVIGATION,
        MUSIC,
        NEWS,
        TRAFFIC_INFOMATION
    }
    public MenuBean(int name, int icon,Type type) {
        this.name = name;
        this.icon = icon;
        this.type = type;
    }

    @Override
    public String toString() {
        return "MenuBean{" +
                "name='" + name + '\'' +
                ", icon=" + icon +
                '}';
    }
}
