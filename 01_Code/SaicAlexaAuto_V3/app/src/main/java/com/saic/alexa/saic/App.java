package com.saic.alexa.saic;


import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.saic.alexa.LVCInteractionService;
import com.saic.alexa.SampleApplication;
import com.saic.alexa.saic.base.Constants;
import com.saic.alexa.saic.util.BroadcastUtil;
import com.saic.alexa.saic.util.LogUtil;

public class App {
    private static final String TAG = "App";
    private LVCConfigReceiver mLVCConfigReceiver;
    private AlexaVoiceService mEngine;
    private Application mContext;
    private CommonBroadcastReceiver mCommonBrocastReceiver;

    public void onCreate(Application sampleApplication) {
        this.mContext = sampleApplication;
        if (!Constants.START_SERVICE_CONFIG) {
            mEngine = new AlexaVoiceService();
            mLVCConfigReceiver = new LVCConfigReceiver();
            IntentFilter filter = new IntentFilter(LVCInteractionService.LVC_RECEIVER_INTENT);
            filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            LocalBroadcastManager.getInstance(SampleApplication.get()).registerReceiver(mLVCConfigReceiver, filter);
        }

        mCommonBrocastReceiver = new CommonBroadcastReceiver();
        BroadcastUtil.registerRequestResultBroadcast(mContext, mCommonBrocastReceiver);
    }

    class LVCConfigReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LVCInteractionService.LVC_RECEIVER_INTENT.equals(intent.getAction())) {
                if (intent.hasExtra(LVCInteractionService.LVC_RECEIVER_FAILURE_REASON)) {
                    // LVCInteractionService was unable to provide config from LVC
                    String reason = intent.getStringExtra(LVCInteractionService.LVC_RECEIVER_FAILURE_REASON);
                    mEngine.onLVCConfigReceived(SampleApplication.get(), null);
                    LogUtil.e(TAG, "Failed to init LVC: " + reason);
                } else if (intent.hasExtra(LVCInteractionService.LVC_RECEIVER_CONFIGURATION)) {
                    // LVCInteractionService received config from LVC
                    LogUtil.i(TAG, "Received config from LVC, starting engine now");
                    String config = intent.getStringExtra(LVCInteractionService.LVC_RECEIVER_CONFIGURATION);
                    mEngine.onLVCConfigReceived(SampleApplication.get(), config);
                }
            }
        }
    }

    /**
     * 当听到关键词时，接收广播，进入到结果显示页面
     */
    private class CommonBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null == intent) {
                return;
            }
            String action = intent.getAction();
            if ("com.saic.alexa.wake.word.detected".equals(action)) {
                Intent result = new Intent(mContext, ResultDetailActivity.class);
                result.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(result);
            }
        }
    }
}
