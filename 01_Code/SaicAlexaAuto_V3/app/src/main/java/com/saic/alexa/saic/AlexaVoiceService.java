package com.saic.alexa.saic;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.amazon.aace.alexa.AlexaProperties;
import com.amazon.aace.alexa.config.AlexaConfiguration;
import com.amazon.aace.core.CoreProperties;
import com.amazon.aace.core.Engine;
import com.amazon.aace.core.config.ConfigurationFile;
import com.amazon.aace.core.config.EngineConfiguration;
import com.amazon.aace.storage.config.StorageConfiguration;
import com.amazon.aace.vehicle.config.VehicleConfiguration;
import com.amazon.sampleapp.core.FileUtils;
import com.amazon.sampleapp.core.ModuleFactoryInterface;
import com.amazon.sampleapp.core.SampleAppContext;
import com.saic.alexa.R;
import com.saic.alexa.saic.data.DataModel;
import com.saic.alexa.saic.impl.AddressBook.AddressBookHandler;
import com.saic.alexa.saic.impl.Alerts.AlertsHandler;
import com.saic.alexa.saic.impl.AlexaClient.AlexaClientHandler;
import com.saic.alexa.saic.impl.AlexaSpeaker.AlexaSpeakerHandler;
import com.saic.alexa.saic.impl.Audio.AudioInputProviderHandler;
import com.saic.alexa.saic.impl.Audio.AudioOutputProviderHandler;
import com.saic.alexa.saic.impl.AudioPlayer.AudioPlayerHandler;
import com.saic.alexa.saic.impl.CBL.CBLHandler;
import com.saic.alexa.saic.impl.CarControl.CarControlDataProvider;
import com.saic.alexa.saic.impl.CarControl.CarControlHandler;
import com.saic.alexa.saic.impl.DoNotDisturb.DoNotDisturbHandler;
import com.saic.alexa.saic.impl.EqualizerController.EqualizerConfiguration;
import com.saic.alexa.saic.impl.EqualizerController.EqualizerControllerHandler;
import com.saic.alexa.saic.impl.ExternalMediaPlayer.MACCPlayer;
import com.saic.alexa.saic.impl.GlobalPreset.GlobalPresetHandler;
import com.saic.alexa.saic.impl.LocalMediaSource.AMLocalMediaSource;
import com.saic.alexa.saic.impl.LocalMediaSource.BluetoothLocalMediaSource;
import com.saic.alexa.saic.impl.LocalMediaSource.CDLocalMediaSource;
import com.saic.alexa.saic.impl.LocalMediaSource.DABLocalMediaSource;
import com.saic.alexa.saic.impl.LocalMediaSource.FMLocalMediaSource;
import com.saic.alexa.saic.impl.LocalMediaSource.LineInLocalMediaSource;
import com.saic.alexa.saic.impl.LocalMediaSource.SatelliteLocalMediaSource;
import com.saic.alexa.saic.impl.LocalMediaSource.USBLocalMediaSource;
import com.saic.alexa.saic.impl.LocationProvider.LocationProviderHandler;
import com.saic.alexa.saic.impl.Logger.LoggerHandler;
import com.saic.alexa.saic.impl.Navigation.NavigationHandler;
import com.saic.alexa.saic.impl.NetworkInfoProvider.NetworkInfoProviderHandler;
import com.saic.alexa.saic.impl.Notifications.NotificationsHandler;
import com.saic.alexa.saic.impl.PhoneCallController.PhoneCallControllerHandler;
import com.saic.alexa.saic.impl.PlaybackController.PlaybackControllerHandler;
import com.saic.alexa.saic.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.saic.alexa.saic.impl.SpeechSynthesizer.SpeechSynthesizerHandler;
import com.saic.alexa.saic.impl.TemplateRuntime.TemplateRuntimeHandler;
import com.saic.alexa.saic.util.BroadcastUtil;
import com.saic.alexa.saic.util.LogUtil;
import com.saic.alexa.saic.util.SoundPoolHelper;

import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class AlexaVoiceService implements Observer {
    private final static String TAG = "AlexaVoiceService";
    private Context mContext;
    private Engine mEngine;
    private SharedPreferences mPreferences;
    private LoggerHandler mLogger;
    private AlexaClientHandler mAlexaClient;
    private AudioInputProviderHandler mAudioInputProvider;
    private AudioOutputProviderHandler mAudioOutputProvider;
    private LocationProviderHandler mLocationProvider;
    private PhoneCallControllerHandler mPhoneCallController;
    private PlaybackControllerHandler mPlaybackController;
    private SpeechRecognizerHandler mSpeechRecognizer;
    private AudioPlayerHandler mAudioPlayer;
    private SpeechSynthesizerHandler mSpeechSynthesizer;
    private TemplateRuntimeHandler mTemplateRuntime;
    private AlexaSpeakerHandler mAlexaSpeaker;
    private AlertsHandler mAlerts;
    private NetworkInfoProviderHandler mNetworkInfoProvider;
    private CBLHandler mCBLHandler;
    private NavigationHandler mNavigation;
    private NotificationsHandler mNotifications;
    private DoNotDisturbHandler mDoNotDisturb;
    private AddressBookHandler mAddressBook;
    private EqualizerControllerHandler mEqualizerControllerHandler;
    private CarControlHandler mCarControlHandler;
    private MACCPlayer mMACCPlayer;
    private CDLocalMediaSource mCDLocalMediaSource;
    private DABLocalMediaSource mDABLocalMediaSource;
    private AMLocalMediaSource mAMLocalMediaSource;
    private FMLocalMediaSource mFMLocalMediaSource;
    private BluetoothLocalMediaSource mBTLocalMediaSource;
    private LineInLocalMediaSource mLILocalMediaSource;
    private SatelliteLocalMediaSource mSATRADLocalMediaSource;
    private USBLocalMediaSource mUSBLocalMediaSource;
    private GlobalPresetHandler mGlobalPresetHandler;
    private boolean mEngineStarted;
    private SoundPoolHelper mSoundPoolHelper;

    // Earcon Settings
    private boolean mDisableStartOfRequestEarcon;
    private boolean mDisableEndOfRequestEarcon;

    // Lock for Earcons
    private Object mDisableStartOfRequestEarconLock = new Object();
    private Object mDisableEndOfRequestEarconLock = new Object();

    public AlexaVoiceService() {}


    /**********************移植业务逻辑*********************************/

    private void init(Context context) {
        mContext = context;
        // Get shared preferences
        mPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE);
        mSoundPoolHelper = new SoundPoolHelper();
    }

    public void onLVCConfigReceived(Context context, String config) {
        // Initialize AAC engine and register platform interfaces
        try {
            if (!mEngineStarted) {
                startEngine(context, config);
            } else if (DataModel.getInstance().isLogin()) {
                // 已经登录过，并且已经启动，则直接进入到主页面
                BroadcastUtil.sendDestoryMainActivity(mContext);
            }
        } catch (RuntimeException e) {
            Log.e(TAG, "Could not start engine. Reason: " + e.getMessage());
            return;
        }

        // Observe log event changes to update the log view
        mSpeechRecognizer.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object object) {
        if (observable instanceof SpeechRecognizerHandler.AudioCueObservable) {
            if (object.equals(SpeechRecognizerHandler.AudioCueState.START_TOUCH)) {
                synchronized (mDisableStartOfRequestEarconLock) {
                    if( !mDisableStartOfRequestEarcon ) {
                        mSoundPoolHelper.load(mContext, R.raw.med_ui_wakesound_touch).play(false);
                    }
                }
            } else if (object.equals(SpeechRecognizerHandler.AudioCueState.START_VOICE)) {
                synchronized (mDisableStartOfRequestEarconLock) {
                    if( !mDisableStartOfRequestEarcon ) {
                        mSoundPoolHelper.load(mContext, R.raw.med_ui_wakesound).play(false);
                    }
                }
            } else if (object.equals(SpeechRecognizerHandler.AudioCueState.END)) {
                synchronized (mDisableEndOfRequestEarconLock) {
                    if( !mDisableEndOfRequestEarcon ) {
                        mSoundPoolHelper.load(mContext, R.raw.med_ui_endpointing_touch).play(false);
                    }
                }
            }
        }
    }

    private void startEngine(Context context, String json) throws RuntimeException {
        init(context);
        // Create an "appdata" subdirectory in the cache directory for storing application data
        File cacheDir = context.getCacheDir();
        File appDataDir = new File(cacheDir, "appdata");
        File sampleDataDir = new File(cacheDir, "sampledata");

        // Copy certs from assets to certs subdirectory of cache directory
        File certsDir = new File(appDataDir, "certs");
        FileUtils.copyAllAssets(context.getAssets(), "certs", certsDir, false);

        // Copy models from assets to certs subdirectory of cache directory.
        // Force copy the models on every start so that the models on device cache are always the latest
        // from the APK
        File modelsDir = new File(appDataDir, "models");
        FileUtils.copyAllAssets(context.getAssets(), "models", modelsDir, true);

        copyAsset("Contacts.json", new File(sampleDataDir, "Contacts.json"), false);
        copyAsset("NavigationFavorites.json", new File(sampleDataDir, "NavigationFavorites.json"), false);

        // Create AAC engine
        mEngine = Engine.create(context);
        ArrayList<EngineConfiguration> configuration = getEngineConfigurations(json, appDataDir, certsDir, modelsDir);

        // Get extra module factories and add configurations
        Map<String, String> data = new HashMap<String, String>();
        data.put(SampleAppContext.CERTS_DIR, certsDir.getPath());
        data.put(SampleAppContext.MODEL_DIR, modelsDir.getPath());
        data.put(SampleAppContext.PRODUCT_DSN, mPreferences.getString(context.getString(R.string.preference_product_dsn), ""));
        data.put(SampleAppContext.APPDATA_DIR, appDataDir.getPath());
        data.put(SampleAppContext.JSON, json);
        SampleAppContext sampleAppContext = new SampleAppContext(null, null, data);
        List<ModuleFactoryInterface> extraFactories = getExtraModuleFactory();
        configExtraModules(sampleAppContext, extraFactories, configuration);

        EngineConfiguration[] configurationArray = configuration.toArray(new EngineConfiguration[configuration.size()]);
        boolean configureSucceeded = mEngine.configure(configurationArray);
        if (!configureSucceeded) {
            throw new RuntimeException("Engine configuration failed");
        }

        // Create the platform implementation handlers and register them with the engine
        // Logger
        if (!mEngine.registerPlatformInterface(mLogger = new LoggerHandler())) {
            throw new RuntimeException("Could not register Logger platform interface");
        }

        // AlexaClient
        if (!mEngine.registerPlatformInterface(mAlexaClient = new AlexaClientHandler(context, mLogger))) {
            throw new RuntimeException("Could not register AlexaClient platform interface");
        }

        // AudioInputProvider
        if (!mEngine.registerPlatformInterface(mAudioInputProvider = new AudioInputProviderHandler(context, mLogger))) {
            throw new RuntimeException("Could not register AudioInputProvider platform interface");
        }

        // AudioOutputProvider
        if (!mEngine.registerPlatformInterface(mAudioOutputProvider = new AudioOutputProviderHandler(context, mLogger, mAlexaClient))) {
            throw new RuntimeException("Could not register AudioOutputProvider platform interface");
        }

        // Set Output Audio provider now that it is registered
        sampleAppContext.setAudioOutputProvider(mAudioOutputProvider);

        // LocationProvider
        if (!mEngine.registerPlatformInterface(mLocationProvider = new LocationProviderHandler(context, mLogger))) {
            throw new RuntimeException("Could not register LocationProvider platform interface");
        }

        // PhoneCallController
        if (!mEngine.registerPlatformInterface(mPhoneCallController = new PhoneCallControllerHandler(context, mLogger))) {
            throw new RuntimeException("Could not register PhoneCallController platform interface");
        }

        // PlaybackController
        if (!mEngine.registerPlatformInterface(mPlaybackController = new PlaybackControllerHandler(context, mLogger))) {
            throw new RuntimeException("Could not register PlaybackController platform interface");
        }

        // SpeechRecognizer
        if (!mEngine.registerPlatformInterface(mSpeechRecognizer = new SpeechRecognizerHandler(mAlexaClient, context, mLogger, true))) {
            throw new RuntimeException("Could not register SpeechRecognizer platform interface");
        }

        // AudioPlayer
        if (!mEngine.registerPlatformInterface(mAudioPlayer = new AudioPlayerHandler(mLogger, mAudioOutputProvider, mPlaybackController))) {
            throw new RuntimeException("Could not register AudioPlayer platform interface");
        }

        // SpeechSynthesizer
        if (!mEngine.registerPlatformInterface(mSpeechSynthesizer = new SpeechSynthesizerHandler())) {
            throw new RuntimeException("Could not register SpeechSynthesizer platform interface");
        }

        // TemplateRuntime
        if (!mEngine.registerPlatformInterface(mTemplateRuntime = new TemplateRuntimeHandler(mLogger, mPlaybackController))) {
            throw new RuntimeException("Could not register TemplateRuntime platform interface");
        }

        // AlexaSpeaker
        if (!mEngine.registerPlatformInterface(mAlexaSpeaker = new AlexaSpeakerHandler(context, mLogger))) {
            throw new RuntimeException("Could not register AlexaSpeaker platform interface");
        }

        // Alerts
        if (!mEngine.registerPlatformInterface(mAlerts = new AlertsHandler(context, mLogger))) {
            throw new RuntimeException("Could not register Alerts platform interface");
        }

        // NetworkInfoProvider
        if (!mEngine.registerPlatformInterface(mNetworkInfoProvider = new NetworkInfoProviderHandler(context, mLogger, mEngine))) {
            throw new RuntimeException("Could not register NetworkInfoProvider platform interface");
        }

        // CBL
        if (!mEngine.registerPlatformInterface(mCBLHandler = new CBLHandler(context, mLogger))) {
            throw new RuntimeException("Could not register CBL platform interface");
        }

        mAlexaClient.registerAuthStateObserver(mCBLHandler);

        // Navigation
        if (!mEngine.registerPlatformInterface(mNavigation = new NavigationHandler(context, mLogger))) {
            throw new RuntimeException("Could not register Navigation platform interface");
        }

        // Notifications
        if (!mEngine.registerPlatformInterface(mNotifications = new NotificationsHandler(context, mLogger))) {
            throw new RuntimeException("Could not register Notifications platform interface");
        }

        // DoNotDisturb
        if (!mEngine.registerPlatformInterface(mDoNotDisturb = new DoNotDisturbHandler(context, mLogger))) {
            throw new RuntimeException("Could not register DoNotDisturb platform interface");
        } else {
            mAlexaClient.registerAuthStateObserver(mDoNotDisturb);
        }

        // Contacts/NavigationFavorites
        String sampleContactsDataPath = sampleDataDir.getPath() + "/Contacts.json";
        ;
        String sampleNavigationFavoritesDataPath = sampleDataDir.getPath() + "/NavigationFavorites.json";

        // Always use sample data from external storage if available
        File sampleContactsFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Contacts.json");
        File sampleNavigationFavoritesFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/NavigationFavorites.json");

        if (sampleContactsFile.exists()) {
            sampleContactsDataPath = sampleContactsFile.getPath();
        }

        if (sampleNavigationFavoritesFile.exists()) {
            sampleNavigationFavoritesDataPath = sampleNavigationFavoritesFile.getPath();
        }

        if (!mEngine.registerPlatformInterface(mAddressBook = new AddressBookHandler(context, mLogger, sampleContactsDataPath, sampleNavigationFavoritesDataPath))) {
            throw new RuntimeException("Could not register AddressBook platform interface");
        }

        // EqualizerController
        if (!mEngine.registerPlatformInterface(mEqualizerControllerHandler = new EqualizerControllerHandler(context, mLogger))) {
            throw new RuntimeException("Could not register EqualizerController platform interface");
        }

        // AlexaComms Handler

        // LVC Handlers
        if (!mEngine.registerPlatformInterface(mCarControlHandler = new CarControlHandler(context, mLogger))) {
            throw new RuntimeException("Could not register Car Control platform interface");
        }

        mMACCPlayer = new MACCPlayer(context, mLogger, mPlaybackController);
        if (!mEngine.registerPlatformInterface(mMACCPlayer)) {
            Log.i("MACC", "registration failed");
            throw new RuntimeException("Could not register external media player platform interface");
        } else {
            Log.i("MACC", "registration succeeded");
        }
        mMACCPlayer.runDiscovery();

        // Mock CD platform handler
        if (!mEngine.registerPlatformInterface(mCDLocalMediaSource = new CDLocalMediaSource(context, mLogger))) {
            throw new RuntimeException("Could not register Mock CD player Local Media Source platform interface");
        }

        // Mock DAB platform handler
        if (!mEngine.registerPlatformInterface(mDABLocalMediaSource = new DABLocalMediaSource(context, mLogger))) {
            throw new RuntimeException("Could not register Mock DAB player Local Media Source platform interface");
        }

        // Mock AM platform handler
        if (!mEngine.registerPlatformInterface(mAMLocalMediaSource = new AMLocalMediaSource(context, mLogger))) {
            throw new RuntimeException("Could not register Mock AM radio player Local Media Source platform interface");
        }

        // Mock SIRIUSXM platform handler
        /*if ( !mEngine.registerPlatformInterface(
                mSIRUSXMLocalMediaSource = new SiriusXMLocalMediaSource(Context, mLogger)
        ) ) throw new RuntimeException( "Could not register Mock SIRIUSXM player Local Media Source platform interface" );*/

        // Mock FM platform handler
        if (!mEngine.registerPlatformInterface(mFMLocalMediaSource = new FMLocalMediaSource(context, mLogger))) {
            throw new RuntimeException("Could not register Mock FM radio player Local Media Source platform interface");
        }

        // Mock Bluetooth platform handler
        if (!mEngine.registerPlatformInterface(mBTLocalMediaSource = new BluetoothLocalMediaSource(context, mLogger))) {
            throw new RuntimeException("Could not register Mock Bluetooth player Local Media Source platform interface");
        }

        // Mock Line In platform handler
        if (!mEngine.registerPlatformInterface(mLILocalMediaSource = new LineInLocalMediaSource(context, mLogger))) {
            throw new RuntimeException("Could not register Mock Line In player Local Media Source platform interface");
        }

        // Mock Satellite Radio platform handler
        if (!mEngine.registerPlatformInterface(mSATRADLocalMediaSource = new SatelliteLocalMediaSource(context, mLogger))) {
            throw new RuntimeException("Could not register Mock Satellite radio player Local Media Source platform interface");
        }

        // Mock USB platform handler
        if (!mEngine.registerPlatformInterface(mUSBLocalMediaSource = new USBLocalMediaSource(context, mLogger))) {
            throw new RuntimeException("Could not register Mock USB player Local Media Source platform interface");
        }

        // Mock global preset
        if (!mEngine.registerPlatformInterface(mGlobalPresetHandler = new GlobalPresetHandler(context, mLogger))) {
            throw new RuntimeException("Could not register Mock Global Preset platform interface");
        }

        // Register extra modules
        loadPlatformInterfacesAndLoadUI(mEngine, extraFactories, sampleAppContext);

        // Start the engine
        if (!mEngine.start()) {
            throw new RuntimeException("Could not start engine");
        }
        mEngineStarted = true;

        // Check if Amazonlite is supported
        if (mEngine.getProperty(AlexaProperties.WAKEWORD_SUPPORTED).equals("true")) {
            //mSpeechRecognizer.enableWakeWordUI();
        }
        mLogger.postInfo("Wakeword supported: ", mEngine.getProperty(AlexaProperties.WAKEWORD_SUPPORTED));

        //log whether LocationProvider gave a supported country
        mLogger.postInfo("Country Supported: ", mEngine.getProperty(AlexaProperties.COUNTRY_SUPPORTED));

        // Initialize AutoVoiceChrome

        mAddressBook.onInitialize();

        initEarconsSettings();
    }

    private void initEarconsSettings() {
        mDisableStartOfRequestEarcon = false;
        mDisableEndOfRequestEarcon = false;
    }

    private void toggleEndOfRequestState( boolean isChecked ) {
        synchronized ( mDisableEndOfRequestEarconLock ) {
            if ( isChecked )
                mDisableEndOfRequestEarcon = false;
            else
                mDisableEndOfRequestEarcon = true;
        }
    }

    private void toggleStartOfRequestState( boolean isChecked ) {
        synchronized ( mDisableStartOfRequestEarconLock ) {
            if ( isChecked )
                mDisableStartOfRequestEarcon = false;
            else
                mDisableStartOfRequestEarcon = true;
        }
    }

    private void copyAsset(String assetPath, File destFile, boolean force) {
        FileUtils.copyAsset(mContext.getAssets(), assetPath, destFile, force);
    }

    /**
     * Get the configurations to start the Engine
     *
     * @param json       JSON string with LVC config if LVC is supported, null otherwise.
     * @param appDataDir path to app's data directory
     * @param certsDir   path to certificates directory
     * @return List of Engine configurations
     */
    private ArrayList<EngineConfiguration> getEngineConfigurations(String json, File appDataDir, File certsDir, File modelsDir) {
        // Configure the engine
        String productDsn = mPreferences.getString(mContext.getString(R.string.preference_product_dsn), "");
        String clientId = mPreferences.getString(mContext.getString(R.string.preference_client_id), "");
        String productId = mPreferences.getString(mContext.getString(R.string.preference_product_id), "");

        AlexaConfiguration.TemplateRuntimeTimeout[] timeoutList = new AlexaConfiguration.TemplateRuntimeTimeout[]{
                new AlexaConfiguration.TemplateRuntimeTimeout(AlexaConfiguration.TemplateRuntimeTimeoutType.DISPLAY_CARD_TTS_FINISHED_TIMEOUT, 8000),
                new AlexaConfiguration.TemplateRuntimeTimeout(AlexaConfiguration.TemplateRuntimeTimeoutType.DISPLAY_CARD_AUDIO_PLAYBACK_FINISHED_TIMEOUT, 8000),
                new AlexaConfiguration.TemplateRuntimeTimeout(AlexaConfiguration.TemplateRuntimeTimeoutType.DISPLAY_CARD_AUDIO_PLAYBACK_STOPPED_PAUSED_TIMEOUT, 1800000)
        };

        JSONObject config = null;
        ArrayList<EngineConfiguration> configuration = new ArrayList<EngineConfiguration>(Arrays.asList(
                //AlexaConfiguration.createCurlConfig( certsDir.getPath(), "wlan0" ), Uncomment Context line to specify the interface name to use by AVS.
                AlexaConfiguration.createCurlConfig(certsDir.getPath()),
                AlexaConfiguration.createDeviceInfoConfig(productDsn, clientId, productId, "Alexa Auto SDK", "Android Sample App"),
                AlexaConfiguration.createMiscStorageConfig(appDataDir.getPath() + "/miscStorage.sqlite"),
                AlexaConfiguration.createCertifiedSenderConfig(appDataDir.getPath() + "/certifiedSender.sqlite"),
                AlexaConfiguration.createCapabilitiesDelegateConfig(appDataDir.getPath() + "/capabilitiesDelegate.sqlite"),
                AlexaConfiguration.createAlertsConfig(appDataDir.getPath() + "/alerts.sqlite"),
                AlexaConfiguration.createNotificationsConfig(appDataDir.getPath() + "/notifications.sqlite"),
                AlexaConfiguration.createDeviceSettingsConfig(appDataDir.getPath() + "/deviceSettings.sqlite"),
                AlexaConfiguration.createEqualizerControllerConfig(
                        EqualizerConfiguration.getSupportedBands(),
                        EqualizerConfiguration.getMinBandLevel(),
                        EqualizerConfiguration.getMaxBandLevel(),
                        EqualizerConfiguration.getDefaultBandLevels()),

                // Uncomment the below line to specify the template runtime values
                //AlexaConfiguration.createTemplateRuntimeTimeoutConfig( timeoutList ),
                StorageConfiguration.createLocalStorageConfig(appDataDir.getPath() + "/localStorage.sqlite"),

                // Example Vehicle Config
                VehicleConfiguration.createVehicleInfoConfig(new VehicleConfiguration.VehicleProperty[]{
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.MAKE, "Amazon"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.MODEL, "AmazonCarOne"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.TRIM, "Advance"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.YEAR, "2025"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.GEOGRAPHY, "US"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.VERSION, String.format(
                                "Vehicle Software Version 1.0 (Auto SDK Version %s)", mEngine.getProperty(CoreProperties.VERSION))),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.OPERATING_SYSTEM, "Android 8.1 Oreo API Level 26"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.HARDWARE_ARCH, "Armv8a"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.LANGUAGE, "en-US"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.MICROPHONE, "Single, roof mounted"),
                        // If Context list is left blank, it will be fetched by the engine using amazon default endpoint
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.COUNTRY_LIST, "US,GB,IE,CA,DE,AT,IN,JP,AU,NZ,FR"),
                        new VehicleConfiguration.VehicleProperty(VehicleConfiguration.VehiclePropertyType.VEHICLE_IDENTIFIER, "123456789a")
                })
        ));

        String endpointConfigPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/aace.json";
        if (new File(endpointConfigPath).exists()) {
            EngineConfiguration alexaEndpointsConfig = ConfigurationFile.create(Environment.getExternalStorageDirectory().getAbsolutePath() + "/aace.json");
            configuration.add(alexaEndpointsConfig);
            Log.i("getEngineConfigurations", "Overriding endpoints");
        }

        // Provide a car control configuration to the Engine.
        //
        // We check if a car control configuration file (CarControlConfig.json) is on the SD
        // card to override the default configuration generated by
        // CarControlDataProvider.generateCarControlConfig(), else use the default. This logic to
        // conditionally generate a config or use a file would not ordinarily be required in a
        // typical application since the config that an application uses will be known and stable.
        // However, since Context sample application allows facilitating testing by overriding the
        // default car control configuration generated by the application itself, we check if such
        // an override is being used.
        String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String externalCarControlConfigPath = sdCardPath + "/CarControlConfig.json";
        File carControlConfigFile = new File(externalCarControlConfigPath);
        if (carControlConfigFile.exists()) {
            Log.i(TAG, "Using car control config from file on SD card");
            EngineConfiguration carControlConfig = ConfigurationFile.create(carControlConfigFile.getPath());
            configuration.add(carControlConfig);

            // If application is using an external car control configuration file, then the CarControlDataProvider
            // needs to generate initial values by scanning the file and building a model of the power, toggle, range and mode
            // controllers defined.
            try {
                CarControlDataProvider.initialize(carControlConfigFile.getPath());
            } catch (Exception e) {
                Context context = mContext.getApplicationContext();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, e.getMessage(), duration);
                toast.show();
            }
        } else {
            // Use programmatic generation of car control configuration. The corresponding custom
            // assets file that complements the default generated car control config is in the
            // assets directory, and LVCInteractionService will take care of ensuring it is used.
            EngineConfiguration ccConfig = CarControlDataProvider.generateCarControlConfig();
            configuration.add(ccConfig);
        }

        return configuration;
    }

    //Retrieve Factories of extra modules
    private List<ModuleFactoryInterface> getExtraModuleFactory() {
        List<ModuleFactoryInterface> extraModuleFactories = new ArrayList<>();
        try {
            String folderName = "sample-app";
            String factoryKey = "factory";
            String category = "name";
            String[] fileList = mContext.getAssets().list(folderName);
            Log.e("loadPlatformInterfaces", "begin loading");
            for (String f : fileList) {
                InputStream is = mContext.getAssets().open(folderName + "/" + f);
                byte[] buffer = new byte[is.available()];
                is.read(buffer);
                String json = new String(buffer, "UTF-8");
                JSONObject obj = new JSONObject(json);
                if (obj != null) {
                    String factoryName = obj.getJSONObject(factoryKey).getString(category);
                    ModuleFactoryInterface instance = (ModuleFactoryInterface) Class.forName(factoryName).newInstance();
                    extraModuleFactories.add(instance);
                    Log.i(TAG, "load extra module:" + factoryName);
                }
                is.close();
            }
        } catch (Exception e) {
            Log.e("loadPlatformInterfaces", e.getMessage());
        }
        return extraModuleFactories;
    }

    //     Add configuration of extra modules
    private void configExtraModules(SampleAppContext sampleAppContext, List<ModuleFactoryInterface> extraModuleFactories, List<EngineConfiguration> configuration) {
        for (ModuleFactoryInterface moduleFactory : extraModuleFactories) {
            List<EngineConfiguration> moduleConfigs = moduleFactory.getConfiguration(sampleAppContext);
            for (EngineConfiguration moduleConfig : moduleConfigs) {
                Log.e(TAG, moduleConfig.toString());
                configuration.add(moduleConfig);
            }
        }
    }

    //Register platform interfaces and load UI of extra modules
    private void loadPlatformInterfacesAndLoadUI(Engine engine, List<ModuleFactoryInterface> extraModuleFactories, SampleAppContext sampleAppContext) {
        for (ModuleFactoryInterface moduleFactory : extraModuleFactories) {
            //     Inflate any drawer view UIs. Must inflate UI before registering platform interfaces
            //LinearLayout llDisplayData = findViewById( R.id.drawer_linear_layout );
            //LayoutInflater linflater = ( LayoutInflater ) getSystemService( Context.LAYOUT_INFLATER_SERVICE );
//            for ( int layoutResourceNumber : moduleFactory.getLayoutResourceNums() ) {
//                View tempView = linflater.inflate( layoutResourceNumber, null );
//                llDisplayData.addView( tempView );
//            }
//            //     Add adapter fragments
//            List<Fragment> fragments = moduleFactory.getFragments( sampleAppContext );
//            for( Fragment fragment : fragments ) {
//                mAdapter.addFragment( fragment );
//                mAdapter.notifyDataSetChanged();
//            }
//            //     Register platform interfaces
//            List<PlatformInterface> platformInterfaces = moduleFactory.getModulePlatformInterfaces( sampleAppContext );
//            for( PlatformInterface platformInterface : platformInterfaces ) {
//                if ( !engine.registerPlatformInterface( platformInterface ) ) throw new RuntimeException( "Could not register extra module interface" );
//            }
        }
    }

}
