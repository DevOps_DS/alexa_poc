package com.saic.alexa.saic;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.saic.alexa.R;
import com.saic.alexa.saic.base.BaseActivity;
import com.saic.alexa.saic.base.Constants;


public class SaicMainActivity extends BaseActivity {
    private final static String TAG = "WeatherActivity";
    private boolean mIsTalkButtonLongPressed;
    ImageView ivSpeak;
    TextView tvExample;
    Button btnMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saic_main);
        ivSpeak = findViewById(R.id.iv_speak);
        tvExample = findViewById(R.id.tv_example);
        btnMenu = findViewById(R.id.bt_menu);
        initView();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        // single click
        ivSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 进入到声音监听页面
                Intent intent = new Intent(mContext, ResultDetailActivity.class);
                intent.putExtra(Constants.FROM_TOUCH,true);
                startActivity(intent);
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SettingMenuActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
