package com.saic.alexa.saic.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.saic.alexa.R;

//import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    protected BaseActivity mContext;
    ImageView ivNavigation;
    TextView tvTitle;
    FrameLayout content;
    RelativeLayout titleRoot;

    LocalBroadcastManager instance;
    DefaultReceiver mDefaultReceiver;
    boolean mShowTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.base_activity);

        // full screen
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        // context
        this.mContext = this;

        // register local broadcast
        instance = LocalBroadcastManager.getInstance(mContext);
        init();

        // 显示标题
        setShowTitle(true);
    }

    protected void init() {
        titleRoot = findViewById(R.id.title_root);
        content = findViewById(R.id.content);
        ivNavigation = findViewById(R.id.iv_navigation);
        tvTitle = findViewById(R.id.tv_title);

        ivNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.finish();
            }
        });
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


    @Override
    public void setContentView(View view) {
        content.addView(view);
        //ButterKnife.bind(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        LayoutInflater.from(this).inflate(layoutResID, content, true);
        //ButterKnife.bind(this);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        content.addView(view, params);
        //ButterKnife.bind(this);
    }

    protected void setShowTitle(boolean flag) {
        titleRoot.setVisibility(flag ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setTitle(int resid) {
        String string = mContext.getString(resid);
        setTitle(string);
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    /**
     * register broadcast
     *
     * @param action
     */
    public void registerAction(String action) {
        if (mDefaultReceiver == null) {
            mDefaultReceiver = new DefaultReceiver();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(action);
        instance.registerReceiver(mDefaultReceiver, intentFilter);
    }

    /**
     * send broadcast
     *
     * @param intent
     */
    public void sendMessage(Intent intent) {
        instance.sendBroadcast(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != instance && mDefaultReceiver != null) {
            instance.unregisterReceiver(mDefaultReceiver);
        }
    }

    class DefaultReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            dispatchIntent(context, intent);
        }
    }

    private void dispatchIntent(Context context, Intent intent) {

    }
}
