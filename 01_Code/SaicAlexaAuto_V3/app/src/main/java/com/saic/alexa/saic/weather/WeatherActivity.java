package com.saic.alexa.saic.weather;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.saic.alexa.R;
import com.saic.alexa.saic.ResultDetailActivity;
import com.saic.alexa.saic.base.BaseActivity;
import com.saic.alexa.saic.base.Constants;
import com.saic.alexa.saic.util.LogUtil;


public class WeatherActivity extends BaseActivity {

    private final static String TAG = "WeatherActivity";

    ImageView ivSpeak;
    TextView tvExample;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        ivSpeak = findViewById(R.id.iv_speak);
        tvExample = findViewById(R.id.tv_example);
        initTitle();
        initView();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtil.e(TAG,"onNewIntent");
    }

    /**
     * Set up title
     */
    private void initTitle() {
        Intent intent = getIntent();
        if (intent == null) {
            LogUtil.e(TAG, "intent is null!");
            finish();
        }
        String title = intent.getStringExtra(Constants.TITLE_ARGS);
        if (TextUtils.isEmpty(title)) {
            return;
        }
        setTitle(title.toUpperCase());

        String prompt = intent.getStringExtra(Constants.PROMPT_ARGS);
        if (!TextUtils.isEmpty(prompt)) {
            tvExample.setText(prompt);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        // single click
        ivSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 进入到声音监听页面
                Intent intent=new Intent(mContext, ResultDetailActivity.class);
                intent.putExtra(Constants.FROM_TOUCH, true);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
