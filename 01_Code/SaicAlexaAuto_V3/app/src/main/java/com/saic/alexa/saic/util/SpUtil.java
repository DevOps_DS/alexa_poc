package com.saic.alexa.saic.util;


import android.content.Context;

public class SpUtil {
    public static void saveValue(Context context, String key, boolean value) {
        context.getSharedPreferences("saic_value", Context.MODE_PRIVATE)
                .edit().putBoolean(key, value).apply();
    }

    public static boolean getValue(Context context, String key) {
        return context.getSharedPreferences("saic_value", Context.MODE_PRIVATE)
                .getBoolean(key, false);
    }
}
