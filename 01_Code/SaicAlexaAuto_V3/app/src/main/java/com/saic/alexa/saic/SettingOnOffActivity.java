package com.saic.alexa.saic;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.saic.alexa.R;
import com.saic.alexa.saic.MainActivity;
import com.saic.alexa.saic.base.BaseActivity;
import com.saic.alexa.saic.base.Constants;
import com.saic.alexa.saic.data.DataModel;
import com.saic.alexa.saic.impl.CBL.CBLHandler;
import com.saic.alexa.saic.logView.SegmentControl;
import com.saic.alexa.saic.util.SpUtil;


public class SettingOnOffActivity extends BaseActivity {
    SegmentControl mScControl;
    TextView tv;
    String key;
    String content;
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saic_setting_on_off);
        Intent intent = getIntent();
        if (intent != null) {
            String result = intent.getStringExtra(Constants.KEY_TITLE);
            key = intent.getStringExtra(Constants.KEY_KEY);
            content = intent.getStringExtra(Constants.KEY_CONTENT);
            if (!TextUtils.isEmpty(result)) {
                setTitle(result);
            }
        }
        mScControl = findViewById(R.id.sc);
        tv = findViewById(R.id.tv_prompt);

        if (!TextUtils.isEmpty(content)) {
            tv.setText(content);
        }

        boolean value = SpUtil.getValue(mContext, key);
        mScControl.setSelectedIndex(value ? 1 : 0);
        mScControl.setOnSegmentControlClickListener(new SegmentControl.OnSegmentControlClickListener() {
            @Override
            public void onSegmentControlClick(int index) {
                if (TextUtils.isEmpty(key)) {
                    return;
                }
                SpUtil.saveValue(mContext, key, index == 0 ? false : true);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
