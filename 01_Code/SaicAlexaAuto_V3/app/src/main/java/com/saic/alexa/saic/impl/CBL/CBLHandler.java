/*
 * Copyright 2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.saic.alexa.saic.impl.CBL;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;


import com.amazon.aace.alexa.AlexaClient.AuthError;
import com.amazon.aace.alexa.AlexaClient.AuthState;
import com.amazon.aace.cbl.CBL;
import com.saic.alexa.R;
import com.saic.alexa.saic.logView.LogRecyclerViewAdapter;
import com.saic.alexa.saic.DisplayMessageActivity;
import com.saic.alexa.saic.data.DataModel;
import com.saic.alexa.saic.impl.AlexaClient.AuthStateObserver;
import com.saic.alexa.saic.impl.Logger.LoggerHandler;
import com.saic.alexa.saic.util.BroadcastUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class CBLHandler extends CBL implements AuthStateObserver {

    private static final String sTag = "CBL";
    private static CBLHandler mCBLHandler;
    private final LoggerHandler mLogger;
    private final Context mActivity;
    private final Handler mMainThreadHandler;
    private final SharedPreferences mPreferences;
    private boolean flag = false;


    public CBLHandler(Context activity, LoggerHandler logger) {
        mLogger = logger;
        mActivity = activity;
        mMainThreadHandler = new Handler(Looper.getMainLooper());
        mPreferences = activity.getSharedPreferences(
                activity.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        mCBLHandler = this;
    }

    public static CBLHandler getInstance() {
        return mCBLHandler;
    }

    @Override
    public void cblStateChanged(CBLState state, CBLStateChangedReason reason, String url, String code) {
        mLogger.postInfo(sTag, String.format("cblStateChanged:state=%s,reason=%s,url=%s code=%s", state.name(), reason.name(), url, code));

        switch (state) {
            case CODE_PAIR_RECEIVED:
                try {
                    if (flag) {
                        sendCodeMessage(url, code);
                        flag = false;
                    } else {
                        Intent intent = new Intent(mActivity, DisplayMessageActivity.class);
                        intent.putExtra("state", "code_pair_received");
                        intent.putExtra("url", url);
                        intent.putExtra("code", code);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mActivity.startActivity(intent);
                    }
                } catch (Exception e) {
                    mLogger.postError(sTag, e.getMessage());
                }
                break;
            case STARTING:
                break;
            case STOPPING:
                switch (reason) {
                    case CODE_PAIR_EXPIRED:
                        sendExpireMessage();
                        break;
                    case AUTHORIZATION_EXPIRED:
                        try {
                            JSONObject renderJSON = new JSONObject();
                            String expiredMessage = "The token has expired. Log in again.";
                            renderJSON.put("message", expiredMessage);
                            mLogger.postDisplayCard(renderJSON, LogRecyclerViewAdapter.CBL_CODE_EXPIRED);
                        } catch (JSONException e) {
                            mLogger.postError(sTag, e.getMessage());
                        }
                        break;
                    case NONE:
                        // CBL stopped using logout button
                        sendResetMessage();
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void clearRefreshToken() {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(mActivity.getString(R.string.preference_refresh_token), "");
        editor.apply();
    }

    @Override
    public void setRefreshToken(String refreshToken) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(mActivity.getString(R.string.preference_refresh_token), refreshToken);
        editor.apply();
    }

    @Override
    public String getRefreshToken() {
        return mPreferences.getString(mActivity.getString(R.string.preference_refresh_token), "");
    }

    @Override
    public void setUserProfile(String name, String email) {
        mLogger.postInfo(sTag, "User profile details updated.");
    }

    @Override
    public void onAuthStateChanged(final AuthState authState, final AuthError authError) {
        mMainThreadHandler.post(() -> {
            if (authState == AuthState.REFRESHED) {
                mLogger.postInfo(sTag, "REFRESHED.");
                sendRefreshedMessage();
                // 记录已经认证登录了
                DataModel.getInstance().setLogin(true);
                // 销毁首页start的登录页面
                BroadcastUtil.sendDestoryMainActivity(mActivity);
            } else {
                mLogger.postInfo(sTag, "Not REFRESHED.");
            }
        });
    }

    public void startLogin(){
        mLogger.postInfo(sTag, "Login Starting  CBL flow...");
        start();
    }

    public void startCbl() {
        mLogger.postInfo(sTag, "Starting CBL flow...");
        flag = true;
        start();
    }

    public void resetCbl() {
        // 记录取消登录了
        DataModel.getInstance().setLogin(false);
        mLogger.postInfo(sTag, "Resetting CBL flow...");
        reset();
    }

    public void cancelCbl() {
        mLogger.postInfo(sTag, "Cancelling CBL flow...");
        cancel();
    }

    private void sendResetMessage() {
        Intent intent = new Intent("alexa-cbl-reset");
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
    }

    private void sendExpireMessage() {
        Intent intent = new Intent("alexa-cbl-expire");
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
    }

    private void sendCodeMessage(String url, String code) {
        Intent intent = new Intent("alexa-cbl-code");
        intent.putExtra("url", url);
        intent.putExtra("code", code);
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
    }

    private void sendRefreshedMessage() {
        Intent intent = new Intent("alexa-cbl-refreshed");
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
    }
}
