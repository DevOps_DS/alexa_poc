package com.saic.alexa.saic.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.saic.alexa.R;
import com.saic.alexa.saic.base.Constants;
import com.saic.alexa.saic.weather.WeatherActivity;

import java.util.List;


public class PagerFragment extends Fragment {
    private List<MenuBean> mData;
    private Context mContext;
    GridView mGridView;

    public static Fragment newInstance(List<MenuBean> data) {
        PagerFragment pagerFragment = new PagerFragment();
        pagerFragment.setData(data);
        return pagerFragment;
    }

    private void setData(List<MenuBean> data) {
        this.mData = data;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.pager_fragment, container, false);
        mGridView = inflate.findViewById(R.id.gv);
        return inflate;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initGridView();
    }

    private void initGridView() {
        mGridView.setAdapter(new MenuAdpter());
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(mContext, mData.get(position).type.name(), Toast.LENGTH_SHORT).show();
                dispatchClickListener(mData.get(position));
            }
        });
    }

    /**
     * handler per item click
     *
     * @param menuBean
     */
    private void dispatchClickListener(MenuBean menuBean) {
        switch (menuBean.type) {
            case ALEXA_TALENT:
                handlerMenuClick(mContext.getString(R.string.menu_alxa_talents),
                        mContext.getString(R.string.alexa_talent_promt));
                break;
            case WEATHER:
                handlerMenuClick(mContext.getString(R.string.menu_weather),
                        mContext.getString(R.string.weather_prompt));
                break;
            default:
                break;
        }
    }

    /**
     * click weather  menu
     */
    private void handlerMenuClick(String title, String prompt) {
        Intent intent = new Intent(mContext, WeatherActivity.class);
        intent.putExtra(Constants.TITLE_ARGS, title);
        intent.putExtra(Constants.PROMPT_ARGS, prompt);
        mContext.startActivity(intent);
    }

    class MenuAdpter extends BaseAdapter {

        @Override
        public int getCount() {
            return mData == null ? 0 : mData.size();
        }

        @Override
        public Object getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.menu_item_layout, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            // calculate per item height
            calculateItemHeight(convertView);
            MenuBean bean = mData.get(position);
            holder.name.setText(mContext.getString(bean.name));
            holder.icon.setImageResource(bean.icon);
            return convertView;
        }

        private void calculateItemHeight(View convertView) {
            int height = mGridView.getMeasuredHeight();
            if (height > 0) {
                int perItemHeight = (height - mGridView.getVerticalSpacing()) / 2;
                AbsListView.LayoutParams params = (AbsListView.LayoutParams) convertView.getLayoutParams();
                if (null != params && params.height != perItemHeight) {
                    params.height = perItemHeight;
                    convertView.setLayoutParams(params);
                }
            }
        }

        class ViewHolder {
            TextView name;
            ImageView icon;

            public ViewHolder(View view) {
                name = view.findViewById(R.id.name);
                icon = view.findViewById(R.id.icon);
            }
        }
    }
}
