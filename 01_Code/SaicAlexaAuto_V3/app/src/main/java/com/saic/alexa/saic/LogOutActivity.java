package com.saic.alexa.saic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.saic.alexa.R;
import com.saic.alexa.saic.base.BaseActivity;
import com.saic.alexa.saic.data.DataModel;
import com.saic.alexa.saic.impl.CBL.CBLHandler;


public class LogOutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.saic_sign_out_cap);
        setContentView(R.layout.activity_saic_log_out);
        findViewById(R.id.btn_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.finish();
            }
        });
        findViewById(R.id.btn_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 关闭所有的Activity
                DataModel.getInstance().finishActivities();
                // 取消登录
                CBLHandler.getInstance().resetCbl();
                // 进入到主页面
                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
