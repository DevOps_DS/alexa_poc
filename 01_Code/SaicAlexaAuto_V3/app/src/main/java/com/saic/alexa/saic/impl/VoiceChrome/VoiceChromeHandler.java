package com.saic.alexa.saic.impl.VoiceChrome;


import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;

import com.amazon.autovoicechrome.AutoVoiceChromeController;
import com.amazon.autovoicechrome.presenter.AutoVoiceChromePresenter;
import com.amazon.autovoicechrome.util.AutoVoiceChromeState;
import com.saic.alexa.R;
import com.saic.alexa.saic.impl.AlexaClient.AlexaClientHandler;
import com.saic.alexa.saic.impl.SpeechRecognizer.SpeechRecognizerHandler;


public class VoiceChromeHandler {
    private AutoVoiceChromeController mAutoVoiceChromeController;

    public VoiceChromeHandler() {

    }

    /**
     * Set up {@link AutoVoiceChromeController}, registering it with various attention state
     * notifying components, and creating an {@link AutoVoiceChromePresenter} to present the events.
     */
    public void initAutoVoiceChrome(Context context,ViewGroup viewGroup) {
        // Create AutoVoiceChromeController for your application to interact with AutoVoiceChrome.
        mAutoVoiceChromeController = new AutoVoiceChromeController(context.getApplicationContext());
        // Initialize AutoVoiceChromeController by providing layout container on which AutoVoiceChrome view will be added.
        mAutoVoiceChromeController.initialize(viewGroup);
        // Notify the controller about state changes like dialog state, connection status,
        // wakeword enabled/disabled etc.
        // mClient.setAutoVoiceChromeController(mAutoVoiceChromeController);
        // mSpeechRecognizer.setAutoVoiceChromeController(mAutoVoiceChromeController);
    }

    public void onStateChanged(AutoVoiceChromeState state) {
        mAutoVoiceChromeController.onStateChanged(state);
    }

    public boolean isEmpty() {
        return mAutoVoiceChromeController == null;
    }

    public void destory() {
        mAutoVoiceChromeController.onDestroy();
    }
}
