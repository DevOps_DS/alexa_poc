package com.saic.alexa.saic.util;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

public class ImageUtil {
    /**
     * 使用glide加载图片
     *
     * @param iv
     * @param url
     */
    public static void load(ImageView iv, String url) {
        Glide.with(iv.getContext())
                .load(url)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        LogUtil.d("imageutil", "width=" + resource.getIntrinsicWidth() + ",height=" + resource.getIntrinsicHeight());
                        iv.setImageDrawable(resource);
                    }
                });
    }
}
