/*
 * Copyright 2017-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.saic.alexa.saic.impl.AlexaClient;

import android.content.Context;
import com.amazon.aace.alexa.AlexaClient;
import com.amazon.autovoicechrome.util.AutoVoiceChromeState;
import com.saic.alexa.saic.impl.Logger.LoggerHandler;
import com.saic.alexa.saic.util.BroadcastUtil;
import com.saic.alexa.saic.util.LogUtil;

import java.util.HashSet;
import java.util.Set;

public class AlexaClientHandler extends AlexaClient {

    private static final String TAG = AlexaClientHandler.class.getSimpleName();

    private final Context mActivity;
    private final LoggerHandler mLogger;
    private ConnectionStatus mConnectionStatus = ConnectionStatus.DISCONNECTED;

    // List of Authentication observers
    private Set<AuthStateObserver> mObservers;

    // Current AuthState and AuthError
    private AuthState mAuthState;
    private AuthError mAuthError;

    // AutoVoiceChrome controller

    public AlexaClientHandler(Context activity, LoggerHandler logger) {
        mActivity = activity;
        mLogger = logger;
        mObservers = new HashSet<>();
    }

    @Override
    public void dialogStateChanged(final DialogState state) {
        mLogger.postInfo(TAG, "Dialog State Changed. STATE: " + state);

        // Notify dialog state change to AutoVoiceChrome
        if ((getConnectionStatus() == ConnectionStatus.CONNECTED)) {
            LogUtil.i(TAG, "dialog state name=" + state.name());
            BroadcastUtil.sendAutoVoiceChromeState(mActivity, convertToAutoVoiceChromeState(state));
        } else {
            // If Alexa is disconnected or pending and dialog state comes
            // then AutoVoiceChrome should show red bar
            BroadcastUtil.sendAutoVoiceChromeState(mActivity, AutoVoiceChromeState.IN_ERROR);
        }
    }

    @Override
    public void authStateChanged(final AuthState state, final AuthError error) {
        if (error == AuthError.NO_ERROR) {
            mLogger.postInfo(TAG, "Auth State Changed. STATE: " + state);
        } else {
            mLogger.postWarn(TAG, String.format("Auth State Changed. STATE: %s, ERROR: %s",
                    state, error));
        }

        notifyAuthStateObservers(state, error);
        mAuthState = state;
        mAuthError = error;
    }

    @Override
    public void connectionStatusChanged(final ConnectionStatus status,
                                        final ConnectionChangedReason reason) {
        mConnectionStatus = status;
        mLogger.postInfo(TAG, String.format("Connection Status Changed. STATUS: %s, REASON: %s",
                status, reason));

        // 连接状态变化时，及时通知监听页面的状态
        if (status.equals(ConnectionStatus.CONNECTED)) {
            BroadcastUtil.sendAutoVoiceChromeState(mActivity, AutoVoiceChromeState.LISTENING);
        } else {
            BroadcastUtil.sendAutoVoiceChromeState(mActivity, AutoVoiceChromeState.OUT_OF_ERROR);
        }
    }

    public ConnectionStatus getConnectionStatus() {
        return mConnectionStatus;
    }

    public void registerAuthStateObserver(AuthStateObserver observer) {
        synchronized (mObservers) {
            if (observer == null) {
                return;
            }
            mObservers.add(observer);

            // notify newly registered observer with the current state
            observer.onAuthStateChanged(mAuthState, mAuthError);
        }
    }

    public void removeAuthStateObserver(AuthStateObserver observer) {
        synchronized (mObservers) {
            if (observer == null) {
                return;
            }
            mObservers.remove(observer);
        }
    }

    private void notifyAuthStateObservers(AuthState authState, AuthError authError) {
        synchronized (mObservers) {
            for (AuthStateObserver observer : mObservers) {
                observer.onAuthStateChanged(authState, authError);
            }
        }
    }

    /**
     * Convert AlexaClient dialog state to AutoVoiceChrome state
     *
     * @param state DialogState
     * @return AutoVoiceChromeState
     */
    private static AutoVoiceChromeState convertToAutoVoiceChromeState(DialogState state) {
        AutoVoiceChromeState autoVoiceChromeState = AutoVoiceChromeState.UNKNOWN;
        switch (state) {
            case IDLE:
                autoVoiceChromeState = AutoVoiceChromeState.IDLE;
                break;
            case SPEAKING:
                autoVoiceChromeState = AutoVoiceChromeState.SPEAKING;
                break;
            case THINKING:
                autoVoiceChromeState = AutoVoiceChromeState.THINKING;
                break;
            case EXPECTING:
                autoVoiceChromeState = AutoVoiceChromeState.IDLE;
                break;
            case LISTENING:
                autoVoiceChromeState = AutoVoiceChromeState.LISTENING;
                break;
            default:
                LogUtil.i(TAG, "no valid state!!!");

        }
        return autoVoiceChromeState;
    }
}