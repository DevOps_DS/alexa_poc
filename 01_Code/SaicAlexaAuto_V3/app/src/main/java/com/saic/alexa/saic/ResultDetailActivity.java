package com.saic.alexa.saic;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.amazon.autovoicechrome.util.AutoVoiceChromeState;
import com.saic.alexa.R;
import com.saic.alexa.SampleApplication;
import com.saic.alexa.saic.base.BaseActivity;
import com.saic.alexa.saic.base.Constants;
import com.saic.alexa.saic.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.saic.alexa.saic.impl.VoiceChrome.VoiceChromeHandler;
import com.saic.alexa.saic.logView.ConfigureViewHolder;
import com.saic.alexa.saic.logView.ViewHolderBodyTemplate1;
import com.saic.alexa.saic.logView.ViewHolderBodyTemplate2;
import com.saic.alexa.saic.logView.ViewHolderLocalSearchListTemplate1;
import com.saic.alexa.saic.logView.ViewHolderLocalSearchListTemplate2;
import com.saic.alexa.saic.logView.ViewHolderPreviousWaypointsTemplate;
import com.saic.alexa.saic.logView.ViewHolderStartNavigationTemplate;
import com.saic.alexa.saic.logView.ViewHolderTrafficDetailsTemplate;
import com.saic.alexa.saic.logView.ViewHolderWeatherTemplate;
import com.saic.alexa.saic.util.BroadcastUtil;
import com.saic.alexa.saic.util.LogUtil;

import org.json.JSONException;
import org.json.JSONObject;

import static com.saic.alexa.saic.logView.LogRecyclerViewAdapter.BODY_TEMPLATE1;
import static com.saic.alexa.saic.logView.LogRecyclerViewAdapter.BODY_TEMPLATE2;
import static com.saic.alexa.saic.logView.LogRecyclerViewAdapter.LOCAL_SEARCH_DETAIL_TEMPLATE1;
import static com.saic.alexa.saic.logView.LogRecyclerViewAdapter.LOCAL_SEARCH_LIST_TEMPLATE2;
import static com.saic.alexa.saic.logView.LogRecyclerViewAdapter.PREVIOUS_WAYPOINTS_TEMPLATE;
import static com.saic.alexa.saic.logView.LogRecyclerViewAdapter.START_NAVIGATION_TEMPLATE;
import static com.saic.alexa.saic.logView.LogRecyclerViewAdapter.TRAFFIC_DETAILS_TEMPLATE;
import static com.saic.alexa.saic.logView.LogRecyclerViewAdapter.WEATHER_TEMPLATE;


public class ResultDetailActivity extends BaseActivity {
    private final static String TAG = "ResultDetailActivity";
    private LayoutInflater mLayoutInflater;
    private FrameLayout mFLContainer;
    private CardView mCardView;
    private ImageButton mIbStartSpeak;
    private ImageView mIvClose;
    private VoiceChromeHandler mVoiceChromeHandler;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String name = intent.getStringExtra("state");
            AutoVoiceChromeState state = AutoVoiceChromeState.valueOf(name);
            if (mVoiceChromeHandler != null) {
                mVoiceChromeHandler.onStateChanged(state);
            }
            if (state.name().equals(AutoVoiceChromeState.LISTENING.name())) {
                resetResult();
                // 正在识别识别和监听语音时，不显示语音touch图标
                mIvClose.setVisibility(View.GONE);
                mIbStartSpeak.setVisibility(View.INVISIBLE);
            } else if (state.equals(AutoVoiceChromeState.SPEAKING)) {
                // 正在播放语音时，显示语音取消图标
                mIvClose.setVisibility(View.VISIBLE);
            } else if (state.equals(AutoVoiceChromeState.IDLE)) {
                // 当语音识别结束后，显示语音touch图标
                mIvClose.setVisibility(View.GONE);
                mIbStartSpeak.setVisibility(View.VISIBLE);
            } else {
                mIvClose.setVisibility(View.GONE);
            }
        }
    };

    private BroadcastReceiver mJsonReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            resetResult();
            dispatchTemplateVisible(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);
        mLayoutInflater = LayoutInflater.from(mContext);

        // Initialize weather templates
        mCardView = findViewById(R.id.saic_container);
        mFLContainer = findViewById(R.id.fl_content);
        mIbStartSpeak = findViewById(R.id.ib_start_speak);
        mIvClose = findViewById(R.id.iv_close);
        mIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // todo 关闭语音播报
                BroadcastUtil.sendStopSpeaking(SampleApplication.get());
            }
        });

        // 点击事件
        mIbStartSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 点击开始说话
                SpeechRecognizerHandler.getInstance().onTapToTalk();
            }
        });

        // 初始化VoiceChromeHandler
        mVoiceChromeHandler = new VoiceChromeHandler();
        mVoiceChromeHandler.initAutoVoiceChrome(this, mFLContainer);
        mVoiceChromeHandler.onStateChanged(AutoVoiceChromeState.IDLE);

        BroadcastUtil.registerAutoVoiceChromeStateBroadcast(this, mReceiver);
        BroadcastUtil.registerJsonResultShowBroadcast(this, mJsonReceiver);

        handlerLogic();
    }

    private void resetResult() {
        mCardView.setVisibility(View.VISIBLE);
        mCardView.removeAllViews();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handlerLogic();
    }

    @SuppressLint("NewApi")
    private void handlerLogic() {
        // 结果页面不可见
        mCardView.setBackgroundColor(getColor(R.color.colorBackground));
        mCardView.setVisibility(View.INVISIBLE);
        mCardView.removeAllViews();
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }

        // 如果是从点击Icon过来的，所以开始进入监听识别状态
        mIbStartSpeak.setVisibility(View.INVISIBLE);
        boolean fromTouch = intent.getBooleanExtra(Constants.FROM_TOUCH, false);
        if (fromTouch && SpeechRecognizerHandler.getInstance() != null) {
            SpeechRecognizerHandler.getInstance().onTapToTalk();
        } else {
            // 进入到这个页面，就开始监听
            mVoiceChromeHandler.onStateChanged(AutoVoiceChromeState.LISTENING);
        }
    }

    private void dispatchTemplateVisible(Intent intent) {
        if (null == intent) {
            LogUtil.e(TAG, "intent is null!");
            finish();
        }
        if (!intent.hasExtra(Constants.TEMPLATE_DATA) || !intent.hasExtra(Constants.TEMPLATE_TYPE)) {
            LogUtil.e(TAG, "intent has no data and type!");
            finish();
        }

        String entry = intent.getStringExtra(Constants.TEMPLATE_DATA);
        int type = intent.getIntExtra(Constants.TEMPLATE_TYPE, -1);
        // Check data
        if (TextUtils.isEmpty(entry) || type == -1) {
            LogUtil.e(TAG, "data is valid!,entry=" + entry);
            finish();
        }

        LogUtil.e("mJsonReceiver", "json=" + entry);
        // 根据类型，加载对应的结果展示模板
        try {
            JSONObject object = new JSONObject(entry);
            switch (type) {
                case BODY_TEMPLATE1:
                    mLayoutInflater.inflate(R.layout.saic_card_body_template1, mCardView, true);
                    ConfigureViewHolder.configureBodyTemplate1(new ViewHolderBodyTemplate1(mCardView), object);
                    break;
                case BODY_TEMPLATE2:
                    mLayoutInflater.inflate(R.layout.saic_card_body_template2, mCardView, true);
                    ConfigureViewHolder.configureBodyTemplate2(new ViewHolderBodyTemplate2(mCardView), object);
                    break;
                case WEATHER_TEMPLATE:
                    mLayoutInflater.inflate(R.layout.saic_card_weather_template, mCardView, true);
                    ConfigureViewHolder.configureWeatherTemplate(new ViewHolderWeatherTemplate(mCardView), object);
                    break;
                case LOCAL_SEARCH_LIST_TEMPLATE2:
                    mLayoutInflater.inflate(R.layout.saic_card_local_search_list_template2, mCardView, true);
                    ViewHolderLocalSearchListTemplate2 viewHolderLocalSearchListTemplate2 = new ViewHolderLocalSearchListTemplate2(mCardView, mLayoutInflater);
                    ConfigureViewHolder.configureLocalSearchListTemplate2(viewHolderLocalSearchListTemplate2, object);
                    setTitle(viewHolderLocalSearchListTemplate2.getTitle().getText().toString());
                    break;
                case LOCAL_SEARCH_DETAIL_TEMPLATE1:
                    mLayoutInflater.inflate(R.layout.card_local_search_list_template1, mCardView, true);
                    ConfigureViewHolder.configureLocalSearchListTemplate1(new ViewHolderLocalSearchListTemplate1(mCardView, mLayoutInflater), object);
                    break;
                case START_NAVIGATION_TEMPLATE:
                    mLayoutInflater.inflate(R.layout.saic_card_start_navigation_template, mCardView, true);
                    ViewHolderStartNavigationTemplate navigationTemplate = new ViewHolderStartNavigationTemplate(mCardView, false);
                    ConfigureViewHolder.configureStartNavigationTemplate(navigationTemplate, object);
                    break;
                case TRAFFIC_DETAILS_TEMPLATE:
                    mLayoutInflater.inflate(R.layout.saic_card_traffic_details_template, mCardView, true);
                    ViewHolderTrafficDetailsTemplate viewHolder = new ViewHolderTrafficDetailsTemplate(mCardView);
                    ConfigureViewHolder.configureTrafficDetailsTemplate(viewHolder, object);
                    break;
                case PREVIOUS_WAYPOINTS_TEMPLATE:
                    mLayoutInflater.inflate( R.layout.saic_card_previous_waypoints_template, mCardView, true );
                    ViewHolderPreviousWaypointsTemplate previousWaypointsTemplate = new ViewHolderPreviousWaypointsTemplate( mCardView );
                    ConfigureViewHolder.configurePreviousWaypointsTemplate(previousWaypointsTemplate, object);
                    break;
//            case TEXT_LOG:
//                view = inflater.inflate( R.layout.log_item, viewGroup, false );
//                viewHolder = new ViewHolderTextLog( view );
//                break;
//            case LIST_TEMPLATE1:
//                view = inflater.inflate( R.layout.log_card_container, viewGroup, false );
//                cardContainer = view.findViewById( R.id.container );
//                inflater.inflate( R.layout.card_list_template1, cardContainer, true );
//                viewHolder = new ViewHolderListTemplate1( view, inflater );
//                break;
//            case LOCAL_SEARCH_LIST_TEMPLATE1:
//                view = inflater.inflate( R.layout.log_card_container, viewGroup, false );
//                cardContainer = view.findViewById( R.id.container );
//                inflater.inflate( R.layout.card_local_search_list_template1, cardContainer, true );
//                viewHolder = new ViewHolderLocalSearchListTemplate1( view, inflater );
//                break;
//            case RENDER_PLAYER_INFO:
//                view = inflater.inflate( R.layout.log_card_container, viewGroup, false );
//                cardContainer = view.findViewById( R.id.container );
//                inflater.inflate( R.layout.card_render_player_info, cardContainer, true );
//                viewHolder = new ViewHolderRenderPlayerInfo( view );
//                break;
//            case CBL_CODE:
//                view = inflater.inflate( R.layout.log_card_container, viewGroup, false );
//                cardContainer = view.findViewById( R.id.container );
//                inflater.inflate( R.layout.card_lwa_cbl, cardContainer, true );
//                viewHolder = new ViewHolderCBLCard( view, mContext );
//                break;
//            case CBL_CODE_EXPIRED:
//                view = inflater.inflate( R.layout.log_card_container, viewGroup, false );
//                cardContainer = view.findViewById( R.id.container );
//                inflater.inflate( R.layout.card_lwa_cbl_expired, cardContainer, true );
//                viewHolder = new ViewHolderCBLExpiredCard( view );
//                break;
//            case JSON_TEXT:
//                view = inflater.inflate( R.layout.log_item, viewGroup, false );
//                viewHolder = new ViewHolderTextLog( view );
//                break;

                default:
                    LogUtil.e(TAG, "no valid type,type=" + type);
                    break;
            }
        } catch (JSONException e) {
            Log.e(TAG, "error:", e);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVoiceChromeHandler.destory();
        BroadcastUtil.unregisterBroadcast(this, mReceiver);
        BroadcastUtil.unregisterBroadcast(this, mJsonReceiver);
    }
}
